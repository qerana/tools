<?php
declare(strict_types=1);
/*
 * The MIT License
 *
 * Copyright 2021 diemarc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace Qerana\Tools;

use ErrorException;
use InvalidArgumentException;
use Qerana\Tools\Exceptions\PathDoesNotExistsException;
use RuntimeException;

/**
 * Utilidades para el manejo de archivos
 *
 * @author diemarc
 */
class File
{
    public

        /**
         * @var string, prefijo a utlizar para el nuevo nombre del archivo copiado
         */
        $prefix = '_cc';
    protected

        /**
         * @var string, path del archivo con q se realizara operaciones
         */
        $file_source,
        /**
         * @var string, nombre con extension del file_source
         */
        $basename,
        /**
         * @var string, nombre sin extension del file_source
         */
        $filename,
        /**
         * @var string, extension del file_source
         */
        $extension,

        /**
         * @var string, path donde copiara o se movera un archivo
         */
        $destination,

        /**
         * @var string, el destination mas el archivo para copiar/mover
         */
        $full_destination;


    /**
     * @throws PathDoesNotExistsException
     */
    public function create(string $base_path, string $filename, string $content = ''): void
    {
        $this->full_destination = $base_path;
        $this->checkBasePath();
//
//        // setting the full path file
//        $file_path = $this->main_path . $this->my_file;
//        // generate new file to perform test
//        $my_file_created = fopen($file_path, "w") or die("Unable to open file!");
//        $txt = "Contenido de prueba\n";
//        fwrite($my_file_created, $txt);
//        fclose($my_file_created);
    }

    /**
     * @throws PathDoesNotExistsException
     */
    private function checkBasePath()
    {

        if (!is_dir($this->full_destination)) {
            throw new PathDoesNotExistsException($this->full_destination);
        }

    }

    /**
     * Obtiene el tipo MIME de un recurso de archivo
     * @return false|string
     */
    public function getMime()
    {
        return mime_content_type($this->file_source);
    }

    /**
     * Mueve o renombra un archivo
     * @param string $destination
     * @param string $new_name
     * @return bool
     */
    public function move(string $destination, string $new_name = ''): bool
    {

        $this->checkFile();
        $this->setDestination($destination, $new_name);

        if (!rename($this->file_source, $this->full_destination)) {
            throw new \RuntimeException('No se ha podido mover:' . $this->file_source . ', a:' . $this->full_destination);
        }

        return true;

    }

    /**
     * Comprueba que el archivo ha sido cargado
     * @throws InvalidArgumentException
     */
    private function checkFile()
    {
        // si esta seteado el source
        if (empty($this->file_source)) {
            throw new InvalidArgumentException('Source file not found, set whit $File->setFile($path)');
        }
    }

    /**
     * Setea el destino para copiar o mover un archivo
     * @throws RuntimeException|ErrorException
     */
    private function setDestination(string $destination, string $new_name = ''): void
    {

        $this->destination = Folder::make($destination);
        $this->filename = (empty($new_name)) ? $this->filename . $this->prefix : $new_name;
        $this->full_destination = $this->destination . '/' . $this->filename;

    }

    /**
     * Copia un archivo a otra ubicación
     * @param string $destination_folder , path donde queremos copiar el archivo
     * @param bool $force , si es true reemplazar el archivo aunque existe, si no arroja exception
     * @param string $new_name , si se envía es el nuevo nombre a usar
     * @return boolean
     * @throws RuntimeException
     * @throws ErrorException
     */
    public function copy(string $destination_folder, bool $force = false,
                         string $new_name = ''): bool
    {
        // si esta seteado el source
        $this->checkFile();

        // formamos el destino
        $this->setDestination($destination_folder, $new_name);
        // si force es true, aunque el destino exista va a reemplazarlo
        if ($force) {

            return copy($this->file_source, $this->full_destination);
        } else {
            $destination_exists = realpath($this->full_destination);

            // si ya existe el archivo destinos
            if ($destination_exists !== false) {
                throw new RuntimeException('File destination already exists:' . $destination_exists . ', u can force the copy with copy($destination,true)');
            } else {

                // sino copiamos
                return copy($this->file_source, $this->full_destination);
            }
        }
    }

    /**
     * Convierte un archivo en base_64
     * @return array
     * @throws InvalidArgumentException , cuando no existe el archivo a convertir
     */
    public function toBase64(): array
    {

        // checheamos si existe el fichero
        $this->checkFile();

        $handle = @fopen($this->file_source, 'r');


        if ($handle === false) {
            throw new RuntimeException('Error at fopen the file');
        }

        $contents = fread($handle, filesize($this->file_source));
        fclose($handle);


        return ['content' => base64_encode($contents), 'filename' => str_replace(" ",
            "_", $this->file_source)];
    }

    /**
     *
     * Reconstruye un archivo desde base64
     * @param string $base64
     * @param string $filename el archivo donde guardar
     * @param string $path_to_save
     * @return $this
     */
    public function fromBase64(string $base64, string $filename, string $path_to_save): File
    {

        // decodificamos el archivo base64
        $binary_doc = base64_decode($base64);

        // ponemos el contenido
        $full_path_file = $path_to_save . '/' . $filename;
        file_put_contents($full_path_file, $binary_doc);


        // seteamos el recurso
        $this->setFile($full_path_file);

        return $this;


    }

    /**
     * Setea un archivo
     * @param string $source
     * @return object
     */
    public function setFile(string $source): object
    {
        $source_path = realpath($source);

        if ($source_path === false) {
            throw new InvalidArgumentException('Source file not found:' . $source);
        }

        $this->file_source = $source_path;

        $this->setSourceInfo();

        return $this;
    }

    /**
     * Obtiene información del source file
     */
    public function setSourceInfo()
    {

        $source_info = pathinfo($this->file_source);
        $this->basename = $source_info['basename'];
        $this->filename = $source_info['filename'];
        $this->extension = $source_info['extension'];
    }

    /**
     * @param string $base64
     * @param string $filename
     * @param string $path_to_save
     * @return $this
     */
    public function fromBase64ToImg(string $base64, string $filename, string $path_to_save): File
    {

        // extract image data from base64 data string
        $pattern = '/data:image\/(.+);base64,(.*)/';
        preg_match($pattern, $base64, $matches);


        // image file extension
        $imageExtension = $matches[1];


        // base64-encoded image data
        $encodedImageData = $matches[2];

        $decodedImageData = base64_decode($encodedImageData);

        // ponemos el contenido
        $full_path_file = $path_to_save . '/' . $filename;
        file_put_contents($full_path_file, $decodedImageData);


        // seteamos el recurso
        $this->setFile($full_path_file);

        return $this;


    }


    public function getFile(){
        return $this->file_source;
    }
}
