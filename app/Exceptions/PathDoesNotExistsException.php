<?php

namespace Qerana\Tools\Exceptions;

class PathDoesNotExistsException extends \Exception
{

    private $path_with_error = '';
    public function __construct(string $fileName)
    {
        $this->path_with_error = $fileName;
        parent::__construct("The provided path does not exists!.");
    }
    public function customMessage()
    {
        echo "The path is- {$this->path_with_error} \n";
    }


}